package page;

import java.io.IOException;

public class PageSerializer {

    public void serialize(int[] containerSerializedPage, Page page) throws IOException {
        byte[][] items = page.getContent().getItems();
        containerSerializedPage[0] = page.getCoordinateX();
        containerSerializedPage[1] = page.getCoordinateY();
        containerSerializedPage[2] = items.length;
        for(int i = 0, j = 3; i<items.length; i++, j++) {
            containerSerializedPage[j++] = items[i][0];
            containerSerializedPage[j] = items[i][1];
        }
    }
    
}
