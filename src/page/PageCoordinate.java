package page;

import triple.Triple;

public class PageCoordinate {
    
    private byte x;
    private byte y;

    public PageCoordinate(byte x, byte y) {
        this.x = x;
        this.y = y;
    }
    
    private PageCoordinate(long x, long y) {
        this.x = (byte) x;
        this.y = (byte) y;
    }

    public byte getX() {
        return x;
    }

    public byte getY() {
        return y;
    }
    
    public static final PageCoordinate fromTriple(Triple triple) {
        return new PageCoordinate(triple.getSubject(), triple.getObject());
    }
    
}
