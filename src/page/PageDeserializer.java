package page;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PageDeserializer {

    public void deserialize(InputStream inputStream, List<Long> tuple) throws IOException {
        long coordinateX = (long)inputStream.read();
        long coordinateY = (long)inputStream.read();
        addTuple(tuple, coordinateX, inputStream, coordinateY);
    }

    private void addTuple(List<Long> tuple, long coordinateX, InputStream inputStream, long coordinateY) throws IOException {
        int itemsQuantity = inputStream.read();
        for(int i = 0; i < itemsQuantity; i++){
            tuple.add((coordinateX << 8) + ((byte)inputStream.read()));
            tuple.add((coordinateY << 8) + ((byte)inputStream.read()));
        }
    }
}
