package page;

import java.io.IOException;
import java.io.OutputStream;
import plane.PlaneCoordinate;

public class Page {
    
    private static final byte X = 0;
    private static final byte Y = 1;
    private static final int SIZE = 2;
    private byte[][] items;
    private final int coordinateX;
    private final int coordinateY;

    public Page(PlaneCoordinate planeCoordinate) {
        this.items = new byte[0][SIZE];
        this.coordinateX = planeCoordinate.getX();
        this.coordinateY = planeCoordinate.getY();
    }

    public int getCoordinateX() {
        return coordinateX;
    }

    public int getCoordinateY() {
        return coordinateY;
    }
    
    public void add(PageCoordinate coordinate) {
        if (check(coordinate)) {
            return;
        }
        int count = count();
        extendTo(count + 1);
        setItem(count, coordinate);

    }

    public void remove(PageCoordinate coordinate) {
        byte index = 0;
        for (int i = 0; i < this.count(); i++) {
            if (existItem(i, coordinate)) {
                continue;
            }
            copyItem(i, index);
            index++;
        }
        extendTo(index);
    }

    public boolean check(PageCoordinate coordinate) {
        for (int i = 0; i < count(); i++) {
            if (existItem(i, coordinate)) {
                return true;
            }
        }
        return false;
    }

    public Content getContent() {
        return new Content(items);
    }
    
    public int size() {
        return (count() * 2) + 2;
    }

    private int count() {
        return this.items.length;
    }

    private boolean existItem(int index, PageCoordinate coordinate) {
        return (items[index][X] == coordinate.getX()) && (items[index][Y] == coordinate.getY());
    }

    private void setItem(int index, PageCoordinate coordinate) {
        items[index][X] = coordinate.getX();
        items[index][Y] = coordinate.getY();
    }

    private void copyItem(int src, int dst) {
        if (src == dst) {
            return;
        }
        items[dst][X] = items[src][X];
        items[dst][Y] = items[src][X];
    }

    private void extendTo(int count) {
        if (count == count()) {
            return;
        }
        byte[][] extension = new byte[count][SIZE];
        System.arraycopy(this.items, 0, extension, 0, Math.min(this.items.length, count));
        this.items = extension;
    }

    public class Content {
        
        public static final byte X = 0;
        public static final byte Y = 1;
        private byte[][] items;

        public Content(byte[][] items) {
            this.items = items;
        }

        public byte[][] getItems() {
            return items;
        }
    }
}
