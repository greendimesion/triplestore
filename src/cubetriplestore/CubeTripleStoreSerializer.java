package cubetriplestore;

import plane.PlaneSerializer;
import plane.Plane;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import triplestore.TripleStore;
import triplestore.TripleStoreSerializer;

public class CubeTripleStoreSerializer extends TripleStoreSerializer {

    private PlaneSerializer planeSerializer;

    public CubeTripleStoreSerializer(TripleStore tripleStore) {
        super(tripleStore);
        this.planeSerializer = new PlaneSerializer();
    }

    @Override
    public void execute(OutputStream outputStream) throws IOException {
        CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
        HashMap<Integer, Plane> planes = cubeTripleStore.getPlanes();
        outputStream.write(planes.size());
        planeSerialize(planes, outputStream);
    }

    private void planeSerialize(HashMap<Integer, Plane> planes, OutputStream outputStream) throws IOException {
        Integer idPlane;
        Plane plane;
        for (Map.Entry<Integer, Plane> entry : planes.entrySet()) {
            idPlane = entry.getKey();
            plane = entry.getValue();
            outputStream.write(idPlane);
            this.planeSerializer.serialize(outputStream, plane);
        }
    }
}
