package cubetriplestore;

import plane.PlaneDeserializer;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import triple.Triple;
import triplestore.TripleStore;
import triplestore.TripleStoreDeserializer;

public class CubeTripleStoreDeserializer extends TripleStoreDeserializer {

    private PlaneDeserializer planeDeserializer;

    public CubeTripleStoreDeserializer(TripleStore tripleStore) {
        super(tripleStore);
        this.planeDeserializer = new PlaneDeserializer();
    }

    @Override
    public void execute(InputStream inputStream) throws IOException{
        getTripleStore().clear();
        CubeTripleStore cubeTripleStore = (CubeTripleStore) getTripleStore();
        addDeserializedTriples(inputStream, cubeTripleStore);

    }

    private void addDeserializedTriples(InputStream inputStream, CubeTripleStore cubeTripleStore) throws IOException {
        int planesQuantity = inputStream.read();
        List<Triple> triple = new ArrayList<>();
        for (int i = 0; i < planesQuantity; i++) {
            planeDeserializer.deserialize(inputStream, triple);
            for (Triple currentTriple : triple) {
                cubeTripleStore.assertTriple(currentTriple);
            }
        }
    }
    
}
