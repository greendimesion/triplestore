package cubetriplestore;

import plane.PlaneCoordinate;
import plane.Plane;
import page.PageCoordinate;
import page.Page;
import java.util.HashMap;
import triple.Triple;
import triplestore.TripleStore;

public class CubeTripleStore extends TripleStore {

    private HashMap<Integer, Plane> planes;

    public CubeTripleStore() {
        this.planes = new HashMap<>();
    }

    @Override
    public void assertTriple(Triple triple){
        Page page = loadPage(triple);
        page.add(PageCoordinate.fromTriple(triple));
    }

    @Override
    public void retractTriple(Triple triple) {
        Page page = loadPage(triple);
        page.remove(PageCoordinate.fromTriple(triple));
    }
    
    @Override
    public void clear() {
        this.planes.clear();
    }

    @Override
    public boolean checkTriple(Triple triple) {
        Page page = getPage(triple);
        if (page == null) return false;
        return page.check(PageCoordinate.fromTriple(triple));
    }
    
    public HashMap<Integer, Plane> getPlanes() {
        return planes;
    }
    
    public Plane getPlane(Triple triple) {
        return planes.get(triple.getPredicate());
    }
    
    private Page getPage(Triple triple) {
        Plane plane = getPlane(triple);
        if (plane == null) return null;
        return plane.get(PlaneCoordinate.fromTriple(triple));
    }
    
    private Page loadPage(Triple triple) {
        Plane plane = loadPlane(triple);
        return plane.load(PlaneCoordinate.fromTriple(triple));
    }

    private Plane loadPlane(Triple triple) {
        Plane plane = planes.get(triple.getPredicate());
        if (plane == null) {
            plane = new Plane();
            planes.put(triple.getPredicate(), plane);
        }
        return plane;
    }
    
}
