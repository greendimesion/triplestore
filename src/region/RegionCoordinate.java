package region;

import triple.Triple;

public class RegionCoordinate {
    private short x;
    private short y;

    public RegionCoordinate(int x, int y) {
        this.x = (short) x;
        this.y = (short) y;
    }

    public short getX() {
        return x;
    }

    public short getY() {
        return y;
    }

    public RegionCoordinate add(RegionCoordinate coordinate) {
       return new RegionCoordinate(x + coordinate.x, y + coordinate.y);
    }

    public RegionCoordinate substract(RegionCoordinate coordinate) {
       return new RegionCoordinate(x - coordinate.x, y - coordinate.y);
    }

    public static RegionCoordinate fromTriple(Triple triple) {
        return new RegionCoordinate((int) triple.getSubject() >> 16, (int) triple.getObject() >> 16);
    }
    
    public boolean match(RegionCoordinate coordinate) {
        return (coordinate.x == this.x) && (coordinate.y == this.y);
    }
}
