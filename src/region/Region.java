package region;

import java.io.OutputStream;
import java.util.ArrayList;

public class Region {    
    public final static int SIZE = 1 << 16;
    private ArrayList<RegionCell> cellList;
    
    public Region() {
        this.cellList = new ArrayList<>();
    }

    public int get(RegionCoordinate coordinate) {
        RegionCell cell = getCell(coordinate);
        if (cell == null) return -1;
        return cell.value;
    }
    
    public void set(RegionCoordinate coordinate, int value) {
        checkOverride(coordinate);
        cellList.add(new RegionCell(coordinate, value));
    }
    
    public int cellQuantity() {
        return this.cellList.size();
    }
    
    public Content getContent() {
        Content content = new Content(cellList.size());
        int index = 0;
        for (RegionCell cell : cellList)
            content.set(index++, cell);
        return content;
    }

    private RegionCell getCell(RegionCoordinate coordinate) {
        for (RegionCell cell : cellList)
            if (cell.match(coordinate)) return cell;
        return null;
    }
    
    private void checkOverride(RegionCoordinate coordinate) throws RuntimeException {
        if (getCell(coordinate) != null) 
            throw new RuntimeException("No se puede sobreescribir el valor");
    }

    private class RegionCell {
        private RegionCoordinate coordinate;
        private int value;

        public RegionCell(RegionCoordinate coordinate, int value) {
            this.coordinate = coordinate;
            this.value = value;
        }
        
        public boolean match(RegionCoordinate coordinate) {
            return coordinate.match(this.coordinate);
        }
    }

    public class Content {
        private int[][] items;
        public static final int X = 0;
        public static final int Y = 1;
        public static final int VALUE = 2;
        private static final int SIZE = 3;

        private Content(int count) {
            this.items = new int[count][SIZE];
        }
        
        private void set(int index, RegionCell cell) {
            items[index][0] = cell.coordinate.getX();
            items[index][1] = cell.coordinate.getY();
            items[index][2] = cell.value;
        }

        public int[][] getItems() {
            return items;
        }
        
    }
    
}
