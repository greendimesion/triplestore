package triplestore;

import java.io.IOException;
import java.io.OutputStream;

public abstract class TripleStoreSerializer {
    
    private TripleStore tripleStore;

    public TripleStoreSerializer(TripleStore tripleStore) {
        this.tripleStore = tripleStore;
    }

    public TripleStore getTripleStore() {
        return tripleStore;
    }
    
    public abstract void execute(OutputStream stream) throws IOException;
    
}
