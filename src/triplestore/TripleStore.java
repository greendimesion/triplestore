package triplestore;

import triple.Triple;

public abstract class TripleStore {

    public abstract void assertTriple(Triple triple);
    public abstract void retractTriple(Triple triple);
    public abstract void clear();
    public abstract boolean checkTriple(Triple triple);
    
}
