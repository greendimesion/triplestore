package triplestore;

import java.io.IOException;
import java.io.InputStream;

public abstract class TripleStoreDeserializer {
    
    private TripleStore tripleStore;

    public TripleStoreDeserializer(TripleStore tripleStore) {
        this.tripleStore = tripleStore;
    }

    public TripleStore getTripleStore() {
        return tripleStore;
    }
    
    public abstract void execute(InputStream stream) throws IOException;
        
}
