package triplestore.query;

import page.Page;

public class PageSearch {
    
    private Page.Content content;

    public PageSearch(Page.Content content) {
        this.content = content;
    }
      
    public int[] searchInRow(int row) {
        return search(Page.Content.X, row);
    }
    
    public int[] searchInColumn(int col) {
        return search(Page.Content.Y, col);
    }

    private int[] search(int dimension, int term) {
        int[] buffer = createBuffer();
        byte[][] items = content.getItems();
        int index = 0;
        for (int i = 0; i < items.length; i++) {
            if (items[i][dimension] != term) continue;
            buffer[index++] = items[i][1-dimension];
        }
        return compact(index, buffer);
    }

    private int[] compact(int size, int[] buffer) {
        int[] result = new int[size];
        System.arraycopy(buffer, 0, result, 0, size);
        return result;
    }

    private int[] createBuffer() {
        return new int[4096];
    }
    
  
    
}
