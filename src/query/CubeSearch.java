package triplestore.query;

import cubetriplestore.CubeTripleStore;
import page.Page;
import page.PageCoordinate;
import plane.Plane;
import plane.PlaneCoordinate;
import region.Region;
import triple.Triple;

public class CubeSearch {
    
    private CubeTripleStore cube;

    public CubeSearch(CubeTripleStore cube) {
        this.cube = cube;
    }
    public Set searchObject(Triple triple) {
        Set set = new Set();
        Plane plane = cube.getPlane(triple);
        Region.Content content = plane.getContent();
        RegionSearch planeSearch = new RegionSearch(content);
        int x = PlaneCoordinate.fromTriple(triple).getLevel(1).getX();
        int[] indexes = planeSearch.searchInRow(x);
        for (int i = 0; i < indexes.length; i++) {
            int index = indexes[i];
            int y = content.getItems()[index][Region.Content.Y];
            int subregion = content.getItems()[index][Region.Content.VALUE];
            Region.Content subcontent = plane.getContent(subregion);
            RegionSearch subSearch = new RegionSearch(content);
            int subx = PlaneCoordinate.fromTriple(triple).getLevel(0).getX();
            int[] pageIndexes = subSearch.searchInRow(subx);
            for (int j = 0; j< pageIndexes.length; j++) {
                int subindex = pageIndexes[j];
                int suby = subcontent.getItems()[subindex][Region.Content.Y];
                Page.Content pageContent = plane.getPageContent(pageIndexes[j]);
                PageSearch pageSearch = new PageSearch(pageContent);
                byte pageX = PageCoordinate.fromTriple(triple).getX();
                int[] tripleIndexes = pageSearch.searchInRow(pageX);
                for (int k = 0; k < tripleIndexes.length; k++) {
                    int tripleIndex = tripleIndexes[k];
                    byte pageY = pageContent.getItems()[tripleIndex][Page.Content.Y];
                    set.add(y << 24 + suby << 8 + pageY);
                }
            }
        }
        return set;
    }

    
}
