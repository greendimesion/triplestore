package triplestore.query;

import region.Region;

public class RegionSearch {
    
    private Region.Content content;

    public RegionSearch(Region.Content content) {
        this.content = content;
    }
      
    public int[] searchInRow(int row) {
        return search(Region.Content.X, row);
    }
    
    public int[] searchInColumn(int col) {
        return search(Region.Content.Y, col);
    }

    private int[] search(int dimension, int term) {
        int[] buffer = createBuffer();
        int[][] items = content.getItems();
        int index = 0;
        for (int i = 0; i < items.length; i++) {
            if (items[i][dimension] != term) continue;
            buffer[index++] = i;
        }
        return compact(index, buffer);
    }

    private int[] compact(int size, int[] buffer) {
        int[] result = new int[size];
        System.arraycopy(buffer, 0, result, 0, size);
        return result;
    }

    private int[] createBuffer() {
        return new int[4096];
    }
    
}
