package plane;

import page.Page;
import java.util.ArrayList;
import region.Region;
import region.RegionCoordinate;

public class Plane {

    private Region region;
    private ArrayList<Region> subregions;
    private ArrayList<Page> pageList;

    public Plane() {
        this.region = new Region();
        this.subregions = new ArrayList<>();
        this.pageList = new ArrayList<>();
    }

    public ArrayList<Page> getPageList() {
        return pageList;
    }

    public int pageListsize() {
        return pageList.size();
    }

    public Page get(PlaneCoordinate coordinate) {
        int index = getIndex(coordinate);
        if (index < 0) {
            return null;
        }
        return pageList.get(index);
    }

    public Page load(PlaneCoordinate coordinate) {
        Page page = get(coordinate);
        if (page != null) {
            return page;
        }
        return createPage(coordinate);
    }

    public Region.Content getContent() {
        return region.getContent();
    }

    public Region.Content getContent(int subregion) {
        return subregions.get(subregion).getContent();
    }

    public Page.Content getPageContent(int index) {
        Page page = pageList.get(index);
        return page.getContent();
    }

    private int getIndex(PlaneCoordinate coordinate) {
        Region subregion = getSubregion(coordinate.getLevel(1));
        if (subregion == null) {
            return -1;
        }
        return subregion.get(coordinate.getLevel(0));
    }

    private void setIndex(PlaneCoordinate coordinate, int index) {
        Region subregion = loadSubRegion(coordinate.getLevel(1));
        subregion.set(coordinate.getLevel(0), index);
    }

    private Region loadSubRegion(RegionCoordinate coordinate) {
        Region subregion = getSubregion(coordinate);
        if (subregion == null) {
            subregion = createSubregion(coordinate);
        }
        return subregion;
    }

    private Region getSubregion(RegionCoordinate coordinate) {
        int index = this.region.get(coordinate);
        if (index < 0) {
            return null;
        }
        return subregions.get(index);
    }

    private Region createSubregion(RegionCoordinate coordinate) {
        Region subregion = new Region();
        region.set(coordinate, subregions.size());
        subregions.add(subregion);
        return subregion;
    }

    private Page createPage(PlaneCoordinate coordinate) {
        Page page = new Page(coordinate);
        setIndex(coordinate, pageList.size());
        pageList.add(page);
        return page;
    }
}
