package plane;

import page.Page;
import page.PageSerializer;
import java.io.IOException;
import java.io.OutputStream;

public class PlaneSerializer {
    
    private PageSerializer pageSerialize;

    public PlaneSerializer() {
        this.pageSerialize = new PageSerializer();
    }
    
    public void serialize(OutputStream outputStream, Plane plane) throws IOException {
        outputStream.write(plane.pageListsize());
        int[] serializedPage;
        for (Page page : plane.getPageList()){
            serializedPage = new int[page.size() + 1];
            pageSerialize.serialize(serializedPage, page);
            for (int value : serializedPage){
                outputStream.write(value);
            }
        }
    }
    
}
