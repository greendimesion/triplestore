package plane;

import java.io.Serializable;
import region.RegionCoordinate;
import triple.Triple;

public class PlaneCoordinate implements Serializable {
    
    private int x;
    private int y;

    public PlaneCoordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    
    public static PlaneCoordinate fromTriple(Triple triple) {
        return new PlaneCoordinate((int) (triple.getSubject() >> 8), (int) (triple.getObject() >>8));
    }

    public PlaneCoordinate add(PlaneCoordinate coordinate) {
       return new PlaneCoordinate(x + coordinate.x, y + coordinate.y);
    }

    public PlaneCoordinate substract(PlaneCoordinate coordinate) {
       return new PlaneCoordinate(x - coordinate.x, y - coordinate.y);
    }
    
    public RegionCoordinate getLevel(int level) {
        int shift = (level << 4);
        return new RegionCoordinate(this.x >> shift, this.y >> shift);
    }

  
}
