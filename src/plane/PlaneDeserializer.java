package plane;

import page.PageDeserializer;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import triple.Triple;

public class PlaneDeserializer {
    
    private PageDeserializer pageDeserialize;

    public PlaneDeserializer() {
        this.pageDeserialize = new PageDeserializer();
    }
    
    public void deserialize(InputStream inputStream, List<Triple> triple) throws IOException {
        int predicate = inputStream.read();
        List<Long> tuple = new ArrayList<>();
        int pagesQuantity = inputStream.read();
        for(int i = 0; i < pagesQuantity; i++) {
            pageDeserialize.deserialize(inputStream, tuple);
            buildTriple(tuple, triple, predicate);
        }
    }

    private void buildTriple(List<Long> tuple, List<Triple> triple, int predicate) {
        for(int i = 0, j = 0; i < (tuple.size()/2); i++) {
            long subject = tuple.get(j++);
            long object = tuple.get(j++);
            triple.add(new Triple(subject, predicate, object));
        }
    }
    
}
