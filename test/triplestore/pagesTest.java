package triplestore;

import triple.Triple;
import cubetriplestore.CubeTripleStore;
import org.junit.Test;
import static org.junit.Assert.*;

public class pagesTest {

    public pagesTest() {
    }

    @Test
    public void add2Triples() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 1));
        store.assertTriple(new Triple(0, 0, 2));
        assertEquals(1, store.getPlane(new Triple(0, 0, 2)).pageListsize());
        assertFalse(store.checkTriple(new Triple(0, 0, 0)));
        assertFalse(store.checkTriple(new Triple(0, 1, 0)));
        assertTrue(store.checkTriple(new Triple(0, 0, 1)));
        assertTrue(store.checkTriple(new Triple(0, 0, 2)));
        assertFalse(store.checkTriple(new Triple(1, 0, 0)));
    }
}