package triplestore;

import triple.Triple;
import cubetriplestore.CubeTripleStore;
import plane.PlaneCoordinate;
import page.Page;
import org.junit.Test;
import static org.junit.Assert.*;

public class PageCoordinateTest {

    public PageCoordinateTest() {
    }

    @Test
    public void addNewPages() {
        CubeTripleStore store = new CubeTripleStore();
        store.assertTriple(new Triple(0, 0, 2));
        Page page = store.getPlane(new Triple(0, 0, 2)).get(PlaneCoordinate.fromTriple(new Triple(0,0,2)));
        long subject = (long)(page.getCoordinateX() << 8) + page.getContent().getItems()[0][0];
        long object = (long)(page.getCoordinateY() << 8) + page.getContent().getItems()[0][1];
        assertEquals(0, subject);
        assertEquals(2, object);
    }
}