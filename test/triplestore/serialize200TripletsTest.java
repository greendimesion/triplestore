package triplestore;

import triple.Triple;
import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreSerializer;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.junit.Test;

public class serialize200TripletsTest {
    
    public serialize200TripletsTest() {
    }
    
    @Test
    public void serialize200Triplets() throws FileNotFoundException, IOException {
        CubeTripleStore store = new CubeTripleStore();
        CubeTripleStoreSerializer cubeSerialize = new CubeTripleStoreSerializer(store);
        File file = new File("serialize200TripletsTest.dat");
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream = new FileOutputStream(file);
        for (int i = 0; i < 1000000; i++) {
           store.assertTriple(createTriple(i));
        }
        cubeSerialize.execute(outputStream);
    }
    
    private Triple createTriple(int i) {
        return new Triple(montecarlo(i,50), 0, montecarlo(i,1000));
    }
    
    private int montecarlo(int center, int range) {
        return (int) (normal() * range / 5 + center);
    }
    
    private int normal() {
        int[] distribution = new int[] {1, 2, 3, 4, 5, 5, 4, 3, 2, 1};
        int value = (int) (Math.random() * 30);
        for (int i = 0; i < 9; i++) {
            if (value < distribution[i]) return i - 5;
            value -= distribution[i];
        }
        return 4;
    }
}