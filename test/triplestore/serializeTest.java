package triplestore;

import triple.Triple;
import cubetriplestore.CubeTripleStore;
import cubetriplestore.CubeTripleStoreSerializer;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.junit.Test;

public class serializeTest {

    public serializeTest() {
    }

    @Test
    public void serialize() throws IOException{
        CubeTripleStore cubeTripleStore = new CubeTripleStore();
        CubeTripleStoreSerializer cubeTripleStoreSerialize = new CubeTripleStoreSerializer(cubeTripleStore);
        File file = new File("serializeTest.dat");
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream = new FileOutputStream(file);
        cubeTripleStore.assertTriple(new Triple(0, 1, 0));
        cubeTripleStore.assertTriple(new Triple(0, 2, 0));
        cubeTripleStore.assertTriple(new Triple(0, 1, 1));
        cubeTripleStoreSerialize.execute(outputStream);
    }    
}
